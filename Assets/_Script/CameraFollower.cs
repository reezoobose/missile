﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Script
{

    public class CameraFollower : MonoBehaviour
    {
        /// <summary>
        /// Fighter Plane Object .
        /// That will be Followed as player .
        /// </summary>
        public GameObject FighterPlane;


        /// <summary>
        /// _Offset is the amount that will be added to control distance between camera and plane .
        /// X and Y Value of Offset force camera to reposition .
        /// </summary>
        [SerializeField]
        private Vector3 _offset;

        /// <summary>
        /// Start the instance.
        /// </summary>
        private void Start()
        {
            _offset = transform.position - FighterPlane.transform.position;
        }
        /// <summary>
        /// Let update the instance.
        /// </summary>
        private void LateUpdate()
        {
            transform.position = FighterPlane.transform.position + _offset;
            


        }
    }
}

