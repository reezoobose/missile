﻿using UnityEngine;

namespace _Script
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class FlightController : MonoBehaviour
    {
        #region Public variable
        /// <summary>
        /// Blast Gameobject .
        /// </summary>
        [Header("Blast Gameobject")]
        public GameObject BlastGameObject;
        /// <summary>
        /// The skay which is added as a child of a fighter plane.
        /// </summary>
        [Header("Sky Gameobject")]
        public GameObject Sky;
        /// <summary>
        /// Thrust to Forward direction.
        /// </summary>
        [Header("Thrust  on the plane ")] public float Thrust;
        /// <summary>
        /// Rotation speed for Fighter plane 
        /// </summary>
        [Header("Speed of Rotation")] public float RotationSpeed = 2f;

        /// <summary>
        /// Flight is Destroyed.
        /// </summary>
        [HideInInspector]public  bool FlightDestroyed = false;

        /// <summary>
        /// Flight controller Reference.
        /// </summary>
        public static  FlightController FlightControllerReference;
        #endregion

        #region Private Variables

        /// <summary>
        /// Rigid body attched to the flight .
        /// </summary>
        private Rigidbody2D _rb;

        

        #endregion

        #region Unity Functions

        /// <summary>
        /// Awake the Instance.
        /// </summary>
        private void Awake()
        {
            //hook the rigid body .
            _rb = gameObject.GetComponent<Rigidbody2D>();
            //create an instance .
            if (FlightControllerReference == null)
            {
                FlightControllerReference = this;
            }



        }
        /// <summary>
        /// Update the instance.
        /// </summary>
        private void FixedUpdate()
        {
            _rb.velocity = transform.up * Thrust;
            Input();
        }

        
       /// <summary>
       /// Late update the instance.
       /// </summary>
       private void LateUpdate()
       {

           //Allow you not to rotate the sky .
           //If sky is used as a child game object.
           Sky.transform.rotation = Quaternion.Euler(0.0f, 0.0f, gameObject.transform.rotation.z * -1.0f);

       }
       

        #endregion

        #region Normal Function

        /// <summary>
        /// Input controller.
        /// </summary>
        private void Input()
        {
            //Movement
            Vector2 dir;
            dir.x = JoystickController.Reference.Horizontal();
            dir.y = JoystickController.Reference.Vertical();
            //gameObject.transform.rotation = Quaternion.Slerp(this.gameObject.transform.rotation, Quaternion.Euler(new Vector3(0, 0, -JoystickController.Reference.AngeOfRotation)),Time.deltaTime * 2f);
            //Rotation
            /*
            _rb.MoveRotation(Mathf.LerpAngle(_rb.rotation, -JoystickController.Reference.AngeOfRotation,
                RotationSpeed * Time.deltaTime)); 
                */
            _rb.rotation = (Mathf.LerpAngle(_rb.rotation, -JoystickController.Reference.AngeOfRotation,
                RotationSpeed * Time.deltaTime));
        }
    /// <summary>
    /// Collision 
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionEnter2D(Collision2D other)
    {
            Debug.Log("Collided with "+other.gameObject.name);

        if (!other.gameObject.CompareTag("Missile")) return;

        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;

        BlastGameObject.SetActive(true);

        FlightDestroyed = true;

    }
    }

    #endregion
}