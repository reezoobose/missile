﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace _Script
{
    /// <summary>
    /// Joy stick Controller .
    /// </summary>
    public sealed class JoystickController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
    {
        #region Public Variables

        /// <summary>
        /// Angle of Rotation.
        /// Set to 180f at the begining to enforce the flight to move negative Y direction .
        /// </summary>
        [NonSerialized]
        public float AngeOfRotation = 180f;

        /// <summary>
        /// Static reference.
        /// </summary>
        public static JoystickController Reference;

        /// <summary>
        /// The value of this vector will be derived for Joystick input.
        /// </summary>
        private Vector3 _derivedOutPutVector;

        /// <summary>
        /// Image that contain circular out line .
        /// </summary>
        public Image CirculerOutLine;

        /// <summary>
        /// Joystick that we are going to move .
        /// </summary>
        public Image Joystick;

        /// <summary>
        /// Public float allowable transform .
        /// </summary>
        [Range(1, 5)] public float JoystickAllowableTransform = 3;

        #endregion

        #region Unity method

        /// <summary>
        /// Awake the instance.
        /// </summary>
        private void Awake()
        {
            if (Reference == null) Reference = this;
        }

        /// <summary>
        /// On pointer down / Touch >mouce click.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerDown(PointerEventData eventData)
        {
            ControllScroll(eventData);
        }

        /// <summary>
        /// On Drag /Finger / Mouse
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            ControllScroll(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            //reset the derived out put.
            _derivedOutPutVector = Vector2.zero;
            //Let the joystick come back to old position.
            Joystick.rectTransform.anchoredPosition = Vector2.zero;
            ;
        }

        #endregion

        #region Normal Function

        /// <summary>
        /// Controll the scroll .
        /// </summary>
        /// <param name="eventData"></param>
        private void ControllScroll(PointerEventData eventData )
        {
            
            //Point in local space of the rect transform.
            //Using C# 4.0 
            //Unable to declair it in inline.
            Vector2 localPoint ;
            //Condition check for the game.
            //User press on the right portion of the screen to control the joystick .
            //circular out line is the image inside that User can touch.
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(CirculerOutLine.rectTransform,
                eventData.position, eventData.pressEventCamera, out localPoint))
            {
                //Debug.Log("You have touched inside the circle ");
                //Debug.Log("Size in x of circular out line Width :"+CirculerOutLine.rectTransform.sizeDelta.x);
                //Debug.Log("Size in Y of circular out line Height :" + CirculerOutLine.rectTransform.sizeDelta.y);
                //We have to make the point like that it should be a point in side the circle of radious 1 .
                //local point devided with image size in order to get a value between o to 1 .
                //Consider the fact : 
                //Total distance is 100 in x direction if you take any point inside that and devide by 100 the value will always 
                //be bound of 0 to 1 .
                localPoint.x = localPoint.x / CirculerOutLine.rectTransform.sizeDelta.x;
                localPoint.y = localPoint.y / CirculerOutLine.rectTransform.sizeDelta.y;
                //All points are now between 0 to 1.
                //Debug.Log("X----After division Range(0,1)-> " + localPoint.x + " Y ---After Division Range(0,1)---> " + localPoint.y);
                //X^2 + Y^2 = 0 is the circle passes through origin and radious is 1.
                //Casting into three dimentional .
                _derivedOutPutVector = new Vector3(localPoint.x , 0f, localPoint.y);
                //Test
                
                //convert the derived vector in a unit vector.
                var unitVector = _derivedOutPutVector.normalized;
                //Debug.Log(_derivedOutPutVector.x + "<========X and Z of The Derived Vector========>" + _derivedOutPutVector.z);
                //in order to clip those values getter than 1 .
                //we should normalize.
                _derivedOutPutVector = _derivedOutPutVector.magnitude > 1f
                    ? _derivedOutPutVector.normalized
                    : _derivedOutPutVector;
                //Debug.Log(_derivedOutPutVector.x + "<=========Normalized=======>" + _derivedOutPutVector.z);
                //Scroll the back ground According to that.
               Parallax.Reference.SetDirectionOfParallax(unitVector.x, unitVector.z);
                //put the joystick at right position.
                Joystick.rectTransform.anchoredPosition = new Vector3(localPoint.x, localPoint.y);
                //Move the joystick Image.
                //JoystickAllowableTransform alow you to move joystick in outer direction.
                Joystick.rectTransform.anchoredPosition = new Vector3(
                    _derivedOutPutVector.x * (CirculerOutLine.rectTransform.sizeDelta.x / JoystickAllowableTransform),
                    _derivedOutPutVector.z *
                    (CirculerOutLine.rectTransform.sizeDelta.y / JoystickAllowableTransform));
                //Calculate the rotation.
                AngeOfRotation = Mathf.Atan2(localPoint.x, localPoint.y)*Mathf.Rad2Deg;
                //Set angle of rotation with lerp.
                //Debug.Log("Angle of rotation ------->"+AngeOfRotation);
            }
        }



        /// <summary>
        /// Horizontal Allow to take input from computer as well  .
        /// </summary>
        /// <returns></returns>
        public float Horizontal()
        {
            return Math.Abs(_derivedOutPutVector.x);
        }

        /// <summary>
        /// Vertical Allow to take input from computer as well  .
        /// </summary>
        /// <returns></returns>
        public float Vertical()
        {
            return Math.Abs(_derivedOutPutVector.z);
        }
        
        #endregion
    }
}