﻿using UnityEngine;

namespace _Script
{
    [RequireComponent(typeof(Rigidbody))]
    public class Missile : MonoBehaviour
    {

        private Rigidbody2D _rb;

        public GameObject Flight;

        public float MissileStartUpSpeed;

        public float MissileMaxSpeed;

        private float _initialDistancebetweenMissileAndFlight = 0.0f;


        public float RotationSpeed;

        private void Awake()
        {
            //catche rigid body.
            _rb = gameObject.GetComponent<Rigidbody2D>();
            //initial distance between missile and flight .
            _initialDistancebetweenMissileAndFlight = ((Vector2)Flight.transform.position - _rb.position).magnitude;


        }

        private void FixedUpdate()
        {
            if (Flight == null) return;

            
                //Speed decay rate.
                var speedDecayRate = Mathf.Abs(_initialDistancebetweenMissileAndFlight / (MissileMaxSpeed - MissileStartUpSpeed));

            //Misiile will move to the flight if flight is present.
            if (FlightController.FlightControllerReference.FlightDestroyed)
            {
                //current distance
                var currentDIstance = ((Vector2) Flight.transform.position - _rb.position).magnitude;
                //distance diff from initial
                var distancedifference = _initialDistancebetweenMissileAndFlight - currentDIstance;
                //we will increase the spped as the missile approach to the flight.
                var optimumSpeed = MissileStartUpSpeed + (distancedifference / speedDecayRate);
                //Providing a velocity in which missile will move.
                //Transform.up is the direction of missile.
                _rb.velocity = (transform.up * (MissileStartUpSpeed + optimumSpeed));
                //Debug.Log("Distance"+ (((Vector2)Flight.transform.position - _rb.position).magnitude + MissileStartUpSpeed) + 
                // "Velocity"+ optimumSpeed );

                //Direction gives the vector in which plane is currently directed.
                //We are normalizing the vector to get an unit vector with the same direction.
                var direction = ((Vector2) Flight.transform.position - _rb.position).normalized;
                //Missile need to follow the plane .
                //In order to do that we have to rotate the missile to the direction of plane .
                var axixOfrotation = Vector3.Cross(direction, transform.up);
                //in 2D plane fligh only rotate in z Direction.
                //So missile will also rotate in z direction.
                var rotateAngle = axixOfrotation.z;
                //Allow to rotate with this velocity.
                _rb.angularVelocity = -rotateAngle * RotationSpeed;


            }
            else
            {
                //current distance
                var currentDIstance = ((Vector2)Flight.transform.position - _rb.position).magnitude;
                //distance diff from initial
                var distancedifference = _initialDistancebetweenMissileAndFlight - currentDIstance;
                //we will increase the spped as the missile approach to the flight.
                var optimumSpeed = MissileStartUpSpeed + (distancedifference / speedDecayRate);
                //Providing a velocity in which missile will move.
                //Transform.up is the direction of missile.
                _rb.velocity = (transform.up * (MissileStartUpSpeed + optimumSpeed));
                //Debug.Log("Distance"+ (((Vector2)Flight.transform.position - _rb.position).magnitude + MissileStartUpSpeed) + 
                // "Velocity"+ optimumSpeed );

                //Direction gives the vector in which plane is currently directed.
                //We are normalizing the vector to get an unit vector with the same direction.
                var direction = ((Vector2)Flight.transform.position - _rb.position).normalized;
                //Missile need to follow the plane .
                //In order to do that we have to rotate the missile to the direction of plane .
                var axixOfrotation = Vector3.Cross(direction, transform.up);
                //in 2D plane fligh only rotate in z Direction.
                //So missile will also rotate in z direction.
                var rotateAngle = axixOfrotation.z;
                //Allow to rotate with this velocity.
                _rb.angularVelocity = -rotateAngle * RotationSpeed;

            }


        }



    }

}
