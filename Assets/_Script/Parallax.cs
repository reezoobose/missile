﻿using System;
using System.Collections;
using System.ComponentModel;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Script
{
    /// <summary>
    ///     provide a parallax effect by providing offset modification.
    ///     Only Attch the script if you have mesh renderer.
    /// </summary>
    /// <remarks>Parallax in any direction</remarks>
    [RequireComponent(typeof(MeshRenderer))]
    public class Parallax : MonoBehaviour
    {
        #region public variable

        /// <summary>
        ///     Static reference.
        /// </summary>
        public static Parallax Reference;

        /// <summary>
        ///     Speed of parallax .
        /// </summary>
        [Header("Parallax Speed")] [Range(-1f, 1f)]
        public float XoffsetSpeed;

        /// <summary>
        ///     Speed of parallax .
        /// </summary>
        [Header("Parallax Speed")] [Range(-1f, 1f)]
        public float YoffsetSpeed;

        /// <summary>
        /// Height and width of the quad.
        /// </summary>
        [HideInInspector]public float Height, Width;

        /// <summary>
        /// Spped of the parallax .
        /// This will be multiply with the orginal speed.
        /// </summary>
        public float Parallaxspeed = 10f;

        #endregion

        #region private variables
        /// <summary>
        ///     Material attched to the gameobject .
        /// </summary>
        private Material _material;

        #endregion

        #region Unity Functions

        /// <summary>
        /// Awake the instance .
        /// </summary>
        private void Awake()
        {
            _material = gameObject.GetComponent<MeshRenderer>().material;
            Reference = this;
            //Height and Width .
            Height = gameObject.transform.localScale.y;
            Width = gameObject.transform.localScale.z;

        }


        
        /// <summary>
        /// Start the Instance.
        /// </summary>
        /// <returns></returns>
        private IEnumerator Start()
        {
            while (!FlightController.FlightControllerReference.FlightDestroyed)
            {
                //Get the offset.
                var offsetInYDirection = _material.mainTextureOffset;
                //Y direction offset update .
                offsetInYDirection.y = offsetInYDirection.y + XoffsetSpeed * Time.deltaTime;
                //X direction offset update .
                offsetInYDirection.x = offsetInYDirection.x + YoffsetSpeed * Time.deltaTime;
                //Impliment the new offset.
                _material.mainTextureOffset = offsetInYDirection;

                yield return null;
            }
        }

        /*===========Avoiding Heavy Update=========
        /// <summary>
        /// Update the instance
        /// </summary>
        private void Update()
        {


            //Get the offset.
            var offsetInYDirection = _material.mainTextureOffset;
            //Y direction offset update .
             offsetInYDirection.y = offsetInYDirection.y + XoffsetSpeed * Time.deltaTime ;
            //X direction offset update .
             offsetInYDirection.x = offsetInYDirection.x + YoffsetSpeed * Time.deltaTime ;
            //Impliment the new offset.
            _material.mainTextureOffset = offsetInYDirection;
               


        }
        ==================*/


        #endregion

        #region Normal Functionsta

        /// <summary>
        ///     Direction in which parallax take place.
        /// </summary>
        /// <param name="xCordinate"></param>
        /// <param name="yCoordinate"></param>
        public void SetDirectionOfParallax(float xCordinate, float yCoordinate)
        {
           
            //set the value.
            XoffsetSpeed = yCoordinate * Parallaxspeed;
            YoffsetSpeed = xCordinate * Parallaxspeed;
            
             /*
            XoffsetSpeed = Mathf.Lerp(XoffsetSpeed, yCoordinate * Parallaxspeed, Time.deltaTime * 10);
            YoffsetSpeed = Mathf.Lerp(YoffsetSpeed, xCordinate * Parallaxspeed, Time.deltaTime * 10);
            */



        }
    }

    #endregion
}
 
 