﻿using UnityEngine;

namespace _Script
{

    /// <summary>
    /// This Class is responsible for moving the background with respect to fligh
    /// </summary>
    [System.Obsolete("No longer used in Game",true)]
    public class Space : MonoBehaviour
    {
        /// <summary>
        /// The Fighter Plane whoes transform will be copied.
        /// </summary>
        public GameObject FlightPlane;

        /// <summary>
        /// Offset of main texture .
        /// </summary>
        private Vector2 _offset;

        /// <summary>
        /// Awake the Instance.
        /// </summary>
        private void Awake()
        {
            //Get the offset.
            _offset = this.gameObject.GetComponent<MeshRenderer>().material.mainTextureOffset;
            //get the transform of flight in X direction and add it to offset.
        }

        private void Update()
        {
            //Now offset is dependent on transform of flight.
            _offset.x += FlightPlane.transform.position.x / 1000;

            //get the transform of flight in Y direction and add it to offset.
            _offset.y += FlightPlane.transform.position.y / 1000;

            //Set offset
            this.gameObject.GetComponent<MeshRenderer>().material.mainTextureOffset = _offset;
        }
    }
}
